class ApiController < ApplicationController

  include ActionController::HttpAuthentication::Basic::ControllerMethods
  http_basic_authenticate_with name: 'test', password: '123test', unless: :dev_or_test?

end
