class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, :with => :not_found
  protect_from_forgery with: :null_session

  def dev_or_test?
    Rails.env == 'development' || Rails.env == 'test'
  end
end
