class Api::V1::LinesController < ApplicationController

  def index
    lines = text_db.get_all
    render json: { lines: lines }, status: 200
  end

  def create
    saved = text_db.insert(string_params[:value])
    status = saved ? 200 : 400
    render json: {}, status: status
  end

  def show
    line = text_db.receive(params[:id])
    status = line ? 200 : 404
    render json: { line: line }, status: status
  end

  private

  def string_params
    params.require(:line).permit(:value)
  end

end
