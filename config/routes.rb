Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :lines, only: [:show, :create, :index]
    end
  end
end
