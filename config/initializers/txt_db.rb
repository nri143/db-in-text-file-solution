class TxtDb
  DB_PATH = "text_database_#{Rails.env}.txt"

  attr_reader :db_path
  attr_accessor :locked

  def initialize
    @db_path = Rails.root + DB_PATH
    File.new(@db_path, 'w') unless File.exists?(@db_path)
    @locked = false
  end

  def insert(record)
    return false if @locked
    return false unless valid?(record)
    @locked = true
    new_record = assign_id(record)
    File.open(@db_path, 'a') do |file|
      file.puts(new_record)
    end
    @locked = false
    true
  end

  def receive(id)
    id = id.to_i
    return if id < 1
    str = IO.readlines(@db_path)[id - 1]
    return unless str
    result = str.match(/{(.*?)}/)
    return unless result
    result[1]
  rescue
    nil
  end

  def get_all
    records = IO.readlines(@db_path)
    return if records.blank?
    records.map { |r| r.match(/{(.*?)}/)[1] }
  rescue
    nil
  end

  def purge
    File.open(@db_path, 'w') do |file|
      file.write ""
    end
  end

  private

  def valid?(record)
    return false if record.blank?
    return false if record.match?(/\n/)
    true
  end

  def assign_id(record)
    last_id = IO.readlines(@db_path).size
    "[#{last_id + 1}] {#{record}}"
  end
end
