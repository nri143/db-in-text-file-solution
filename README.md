# README

Requests:

* POST /api/v1/lines -> add new record
* GET /api/v1/lines -> get array of all lines
* GET /api/v1/lines/:id -> get line by id

Notes:

* Used http_basic_authenticate for authentication, because we don't have registration for users, that's why we have no need to implement another kind of authentication;

Disadvantages:

* No abilities to delete records;
* No abilities to update records;
* No smart error-handling and error description in responses;
* No queues, to handle multiple requests in same time. Currently we return error status code;
* No backups for current database-file (We can store our file in AWS S3, for example);
* No prevention of updating database-file manually;
* In case when we have a lot of records in database-file, it can cause problems to RAM;


