require 'rails_helper'

RSpec.describe Api::V1::LinesController, type: :controller do

  before(:each) do
    text_db.purge
    30.times { |i| text_db.insert("Hello world #{i+1}") }
  end

  describe "#Index" do
    context "DB has records" do
      it "returns records" do
        get :index, format: :json
        expect(response.json['lines'].count).to eql(30)
      end
    end
  end

  describe "#Create" do
    context "Insert valid record" do
      it "creates new record" do
        post :create, format: :json, params: { line: { value: "New value" }}
        expect(response.status).to eql(200)
      end
    end

    context "Insert invalid record" do
      it "returns error status code" do
        post :create, format: :json, params: { line: { value: "New value\n" }}
        expect(response.status).to eql(400)
      end
    end
  end

  describe "#Show" do
    context "Get valid record" do
      it "returns string by id" do
        get :show, format: :json, params: { id: 1 }
        expect(response.json['line']).to eql("Hello world 1")
        expect(response.status).to eql(200)
      end
    end

    context "Get invalid record" do
      it "returns 404 if invalid id" do
        get :show, format: :json, params: { id: 100 }
        expect(response.status).to eql(404)
      end
    end
  end
end
